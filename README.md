# Clean_Light

**Short Description** : A minimal, clean viewing experience for kde plasma 5


![Workflow clean light theme](contents/previews/fullscreenpreview.jpg)


[Link to youtube video showing animations](https://www.youtube.com/watch?v=D3H3G6MwhjY)

**[Items installed automatically]**
1. Papirus Icon theme
2. Breezemite windows decorations
3. Chili for plasma lockscreen
4. No panel layout for latte dock


**[Softwares to be installed Manually]**
1. Latte-Dock
2. Kvantum
3. Fonts : Hack, Noto Sans

Hello everyone, this is the kde plasma experience i promised you a while ago. You can apply this *look-and-feel theme* in two way : 

1. **[REQUIRED]** = This is the *Look* part of the theme. It changes your desktop layout, window decorations, wallpaper, and lockscreen.

2. **[Optional With Effects]** The *Feel* part involves installing some new software - `latte dock` and `kvantum` - and some fonts - Hack, Noto Sans - as well as modifying the configuration of some applications - Dolphin, Konsole etc- to create a more integrated visual appeal. It would change the widget style and colorscheme. This would create a truly modern PC optimized for beauty and productivity.

You can find all modified files in the **custom** folder. The complete list of where to put each modified file is documented under the section [Optional]installing modifications in the file `List of Changes.md`.


## Coming up in future versions : 
1. A script for installation and removal
2. a dark variant of the theme
3. an auto-changing version of the theme, alternating between light and dark, depending on the time of the day

## Firefox color theme : 
here are a couple of very nice color theme i have created in [Firefox Color](https://mozilla.github.io/FirefoxColor/) that blends in perfectly with this theme : 

2. [Clean Light Firefox Theme](https://color.firefox.com/?theme=XQAAAALuAAAAAAAAAABBqYhm849SCiazH1KEGccwS-xNVAWBvyCzAqbmjM7SznRo7-8gXfASMsrLz__qv5vwIV1OwSnq7w2BeMFJgswunk5kpgi0fogPrzrIpCc3lNolTO5Qb6vMzhzG1jMz3IAG_AhvtxFhN5ulYGp7LcPJOW8wbE_D8zZb_zQpt5Bo4fiBnI4otyVaKDArmd1Gj3JNkIQKtgnAQBOlfdvWCDULm_H9wgH_Km6AAA)
![Firefox Clean Light](contents/previews/firefox3.png)


