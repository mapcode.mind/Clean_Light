 ## modifying script 
- [x] change path from absolute to relative in desktop layout
- [ ] change window dimensions from absolute to screen-based
- [ ] make some settings automatic in custom files
- [ ] when modifying things, make backup of original files.

## Finishing touches
- [ ] add credits
- [x] Modify ReadMe
- [x] add License
