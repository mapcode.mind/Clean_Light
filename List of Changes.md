# Clean Light Plasma look-and-feel theme :

Note : the checklisted boxes have already been done. You don't need to do anything for them.

## Simple theme layout
- [x] wallpaper
- [x] analog clock widget
- [x] notes widget
- [x] trash at the bottom right

## installing dependencies 

### 1. from kde store
- [x] [breezemite window decorations dependency](https://www.opendesktop.org/p/1169286/)
- [x] [SDDM Theme chili](https://www.opendesktop.org/p/1214121/)
- [x] [papirus-adapta icons](https://www.opendesktop.org/p/1166289/)
- [x] [No Panel latte dock layout](https://www.opendesktop.org/p/1259838/)

### 2. [Optional]from software repos
- [ ] kvantum
- [ ] latte dock
- [ ] Font : hack, noto sans


## [Optional]installing modifications 
- [ ] To create the notes widget with 3 colors and white bg, put `plasma_notes/` in `~/.local/share/plasma_notes/`
- [ ] SDDM Theme wallpaper - 'bsunseth4k.jpg'
- [ ] Kvantum folder in `~/.config/Kvantum/`
- [ ] modify dolphinrc similar to `~/.config/dolphinrc`
- [ ] konsole colorscheme black on white in `~/.local/share/konsole/`
- [ ] Kwin effects/animations by modifying `~/.config/kwinrc`


