var plasma = getApiVersion(1);

var layout = {
    "desktops": [
        {
            "applets": [
                {
                    "config": {
                        "/": {
                            "PreloadWeight": "0"
                        },
                        "/ConfigDialog": {
                            "DialogHeight": "540",
                            "DialogWidth": "720"
                        },
                        "/General": {
                            "showSecondHand": "true"
                        }
                    },
                    "geometry.height": 8,
                    "geometry.width": 10,
                    "geometry.x": 64,
                    "geometry.y": 0,
                    "plugin": "org.kde.plasma.analogclock",
                    "title": "Analogue Clock"
                },
                {
                    "config": {
                        "/": {
                            "PreloadWeight": "0"
                        },
                        "/ConfigDialog": {
                            "DialogHeight": "540",
                            "DialogWidth": "720"
                        },
                        "/General": {
                            "noteId": "394dfd0f-7573-4df5-ba19-0566e0491e"
                        }
                    },
                    "geometry.height": 12,
                    "geometry.width": 32,
                    "geometry.x": 21,
                    "geometry.y": 10,
                    "plugin": "org.kde.plasma.notes",
                    "title": "Notes"
                }
            ],
            "config": {
                "/": {
                    "formfactor": "0",
                    "immutability": "1",
                    "lastScreen": "0",
                    "wallpaperplugin": "org.kde.image"
                },
                "/ConfigDialog": {
                    "DialogHeight": "540",
                    "DialogWidth": "720"
                },
                "/Configuration": {
                    "PreloadWeight": "0"
                },
                "/General": {
                    "ToolBoxButtonState": "top",
                    "ToolBoxButtonX": "928",
                    "iconSize": "3",
                    "positions": "0,1,desktop:/trash",
                    "pressToMove": "false",
                    "previewPlugins": "djvuthumbnail,exrthumbnail,imagethumbnail,jpegthumbnail,windowsimagethumbnail,gsthumbnail,rawthumbnail,svgthumbnail,ffmpegthumbs",
                    "showToolbox": "false",
                    "sortMode": "-1"
                },
                "/Wallpaper/org.kde.color/General": {
                    "Color": "255,255,255"
                },
                "/Wallpaper/org.kde.image/General": {
                    "Blur": "true",
                    "Color": "255,255,255",
                    "FillMode": "6",
                    "Image": "file://$PWD/custom/wallpapers/ren-ran-61997-unsplash.jpg"
                }
            },
            "wallpaperPlugin": "org.kde.image"
        }
    ],
    "panels": [
    ],
    "serializationFormatVersion": "1"
}
;

plasma.loadSerializedLayout(layout);
